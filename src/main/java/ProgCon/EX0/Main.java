package ProgCon.EX0;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class Main {

	public static void main(String[] args) throws InterruptedException, ExecutionException {
		// TODO Auto-generated method stub
		
		//q1 q2
		String currentThread=Thread.currentThread().getName();
		System.out.println(" Le Thread Courant est : "+ currentThread);
		//q3
		Runnable exacuatble = ()-> System.out.println("J'exe : "+Thread.currentThread().getName());
		Thread newThread = new Thread(exacuatble);
		newThread.start();
		//q4
		Object lock=new Object();
		Callable<String>  exacuatble2 = ()-> {
			synchronized (lock) {
			return Thread.currentThread().getName();
			}
		};
		
		ExecutorService executeur = Executors.newFixedThreadPool(3);
		Future<?> f1 =executeur.submit(exacuatble2);
		Future<String>  f2 =executeur.submit(exacuatble2);
		Future<String>  f3 =executeur.submit(exacuatble2);
		
		System.out.println(f1.get());
		System.out.println(f2.get());
		System.out.println(f3.get());
		
		
		
		executeur.shutdown();
	}

}
