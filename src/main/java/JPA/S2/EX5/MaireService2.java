package JPA.S2.EX5;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;



public interface MaireService2 {
	
	public static void WriteMaireInDataBase(EntityManager EM, Maire2 Maire  ) {
		EntityTransaction transaction = EM.getTransaction();
		transaction.begin();
		EM.persist(Maire);
		transaction.commit();
	}

}
