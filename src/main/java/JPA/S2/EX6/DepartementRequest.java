package JPA.S2.EX6;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;



import JPA.S2.EX5.Commune2;
import JPA.S2.EX5.Maire2;
import JPA.S2.EX5.Maire2.Civilite;


public interface DepartementRequest {
	
	public static int getNumberOfTownWithDepartementNumber(String numDepatement) throws SQLException {

		
		Connection connexionJDBC = DriverManager.getConnection(
				"jdbc:mysql://127.0.0.1:3306/db_jdbc?user=antoine",
				"antoine",
				"user"
				
				);
		
		String sql = "SELECT * FROM db_jpa.Commune2 where departement_numero like '"+numDepatement+"'"; 
		Statement statment = connexionJDBC.createStatement();
		ResultSet result=statment.executeQuery(sql);
		ArrayList<Commune2> listeCommune = new ArrayList<Commune2>();
		while (result.next()) {
			
			Commune2 CurrentCommune = new Commune2();
			//System.out.println(CurrentCommune);
			listeCommune.add(CurrentCommune);
		}
		
		
		return listeCommune.size();
		
		
		
	}
	public static float getRationMenWomen(String numDepatement) throws SQLException {
		
		Connection connexionJDBC = DriverManager.getConnection(
				"jdbc:mysql://127.0.0.1:3306/db_jpa?user=antoine",
				"antoine",
				"user"
				
				);
		
		float nbMaire=getNumberOfTownWithDepartementNumber(numDepatement);
		float SumMaire=0;
		String sql = "select * from Maire2 where id in (select maire_id from Commune2 where departement_numero like '"+numDepatement+"');"; 
		Statement statment = connexionJDBC.createStatement();
		ResultSet result=statment.executeQuery(sql);
		ArrayList<Maire2> listeMaire = new ArrayList<Maire2>();
		while (result.next()) {
			if(result.getString("civilité").equals("1")) {
				SumMaire=SumMaire+1;
			}
				
		}

		return SumMaire/nbMaire;
		
	}
	
public static Maire2 getNumberOldestMayor(String numDepatement) throws SQLException {

		
		Connection connexionJDBC = DriverManager.getConnection(
				"jdbc:mysql://127.0.0.1:3306/db_jpa?user=antoine",
				"antoine",
				"user"
				
				);
		
		String sql = "select * from Maire2 where id in (select maire_id from Commune2 where departement_numero like '"+numDepatement+"');"; 
		Statement statment = connexionJDBC.createStatement();
		ResultSet result=statment.executeQuery(sql);
		ArrayList<Maire2> listeMaire = new ArrayList<Maire2>();
		while (result.next()) {
			
			
			Maire2 currentMaire= new Maire2();
			
			if(result.getString("civilité").equals("0")) {
				currentMaire.setPrenom(result.getString("prenom"));
				currentMaire.setNom(result.getString("nom"));
				currentMaire.setDateDeNaissance(result.getDate(3));
				currentMaire.setCivilité(Civilite.M);
			}else {
				currentMaire.setPrenom(result.getString("prenom"));
				currentMaire.setNom(result.getString("nom"));
				currentMaire.setDateDeNaissance(result.getDate(3));
				currentMaire.setCivilité(Civilite.F);
			}
			
			listeMaire.add(currentMaire);
			//Maire2 name = new Maire2(prenom, nom, Date dateDeNaissance,  civilité);
			//System.out.println(CurrentCommune);
			//listeCommune.add(CurrentCommune);
		}		
		listeMaire.sort((Maire2 m1,Maire2 m2)->m1.getDateDeNaissance().compareTo(m2.getDateDeNaissance()));

		return listeMaire.get(0);

	}

}
