package JPA.S1.EX2;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import org.hibernate.annotations.Type;


@Entity
public class Commune implements Serializable {
	
	String departement;
	String nomCommune;
	@OneToOne (cascade = CascadeType.ALL)
	Maire  maire;
	@Id @GeneratedValue(strategy = GenerationType.SEQUENCE)
	int id;
	
	public Commune(String departement, String nomCommune, Maire maire) {
		
		this.departement = departement;
		this.nomCommune = nomCommune;
		this.maire = maire;
	}

	@Override
	public String toString() {
		return  " nomCommune=" + nomCommune + ", maire= " + maire +", ID= "+id;
	}

	public String getDepartement() {
		return departement;
	}

	public void setDepartement(String departement) {
		this.departement = departement;
	}

	public String getNomCommune() {
		return nomCommune;
	}

	public void setNomCommune(String nomCommune) {
		this.nomCommune = nomCommune;
	}

	public Maire getMaire() {
		return maire;
	}

	public void setMaire(Maire maire) {
		this.maire = maire;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Commune() {
		
	}
	
	

}
