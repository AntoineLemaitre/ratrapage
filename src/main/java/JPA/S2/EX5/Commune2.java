package JPA.S2.EX5;

import java.io.Serializable;

import javax.persistence.Access;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.AccessType;

import org.hibernate.annotations.Type;

import JPA.S1.EX2.Maire;


@Entity
/*
@Access(AccessType.FIELD)
@NamedQueries({

	@NamedQuery(name = "getCommuneWithName()", query = "SELECT * from Commune2 where nomCommune like 'SANNOIS'"),
 
	})
*/
public class Commune2 implements Serializable {
	
	@OneToOne 
	Departement2 departement;
	String nomCommune;
	@OneToOne (cascade =CascadeType.ALL)
	Maire2  maire;
	@Id @GeneratedValue(strategy = GenerationType.SEQUENCE)
	int id;
	
	public Commune2(Departement2 departement, String nomCommune, Maire2 currentLineMaire) {
		
		this.departement = departement;
		this.nomCommune = nomCommune;
		this.maire = currentLineMaire;
	}

	@Override
	public String toString() {
		return  " nomCommune=" + nomCommune + ", maire= " + maire +", ID= "+id;
	}

	public Departement2 getDepartement() {
		return departement;
	}

	public void setDepartement(Departement2 departement) {
		this.departement = departement;
	}

	public String getNomCommune() {
		return nomCommune;
	}

	public void setNomCommune(String nomCommune) {
		this.nomCommune = nomCommune;
	}

	public Maire2 getMaire() {
		return maire;
	}

	public void setMaire(Maire2 maire) {
		this.maire = maire;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Commune2() {
		
	}
	
	

}
