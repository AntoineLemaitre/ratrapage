package JPA.S1.EX2;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class CommuneReader {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		EntityManagerFactory EMF = Persistence.createEntityManagerFactory("jpa-test");
		System.out.println("EMF= "+EMF);
		EntityManager EM = EMF.createEntityManager();
		Commune CommuneRead=EM.find(Commune.class, 1);
		System.out.println(CommuneRead);
		
	}

}
