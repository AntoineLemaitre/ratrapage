package JPA.S1.EX2;

import java.io.Serializable;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.print.attribute.HashAttributeSet;

@Entity
public class Departement implements Serializable{
	
	@Id 
	String numero;
	String nom;
	@ManyToMany
	Set<Commune> communes= new HashSet<Commune>();
	
	public String getNumero() {
		return numero;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public Departement() {
		super();
	}
	public Departement(String numero, String nom) {
		super();
		this.numero = numero;
		this.nom = nom;
	}
	@Override
	public String toString() {
		return "Departement [numero=" + numero + ", nom=" + nom + "]";
	}
	public Set<Commune> getCommunes() {
		return communes;
	}
	public void setCommunes(Set<Commune> communes) {
		this.communes = communes;
	}
	
	

}
