package JPA.S2.EX5;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import javax.persistence.EntityManager;

import JPA.S2.EX5.Maire2.Civilite;

public interface DepartementImporter2 {
	
	public static void importDepartementFromCSVToDataBase(EntityManager EM, String fileDep, String  fileCommuneAndMaire ) {
		
		String fileName = fileDep;
		Path path = Path.of(fileName);
		List<Departement2> listDepartement = new ArrayList<>();
		
		try (Stream<String> lines = Files.lines(path);) {

			lines.forEach(ligne->{
				String[] strSlpit= ligne.split(";");
				Departement2 currentLineDepartement = new Departement2(strSlpit[0],strSlpit[1]);
				//DepartementService2.WriteCommuneInDataBase(EM, currentLineDepartement);
				listDepartement.add(currentLineDepartement);
			});
			
	
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		fileName = fileCommuneAndMaire;
		path = Path.of(fileName);
		try (BufferedReader reader= Files.newBufferedReader(path)) {
			
			
			
			String line = reader.readLine();
			line = reader.readLine();
			//System.out.println("La !");
			while (line!=null) {
				
				String[] strSplit=line.split(";");
				listDepartement.forEach(dep->{
					if(dep.numero.equals(strSplit[0].substring(1, 3))){
						
						Maire2 CurrentLineMaire = new Maire2(strSplit[5].replace("\"", "").replace("\"", ""), strSplit[4].replace("\"", "").replace("\"", ""),java.sql.Date.valueOf(strSplit[7]),Civilite.valueOf(strSplit[6].replace("\"", "").replace("\"", "")));
						Commune2 CurrentLineCommune = new Commune2(dep, strSplit[2].replace("\"", "").replace("\"", ""), CurrentLineMaire);
						dep.communes.add(CurrentLineCommune);
						
						//MaireService2.WriteMaireInDataBase(EM, CurrentLineMaire);
						//CommuneService2.WriteCommuneInDataBase(EM, CurrentLineCommune);
						//DepartementService2.WriteCommuneInDataBase(EM, dep);
					}
				});	

				line = reader.readLine();
			}
			System.out.println("Wait for Import Ending ..");
				
			listDepartement.forEach(dep->{
				
				DepartementService2.WriteCommuneInDataBase(EM, dep);
				
			});
			System.out.println("END CODE !");
	
		} catch (Exception e) {
			// TODO: handle exception
			
			e.printStackTrace();
			System.out.println("Sortie catch !");
			System.out.println(e);
			
		}
	}

}
