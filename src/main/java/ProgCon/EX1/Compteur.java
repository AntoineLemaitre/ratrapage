package ProgCon.EX1;

public class Compteur {

	Object mutex=new Object();
	int Valeur=0;
	public void Increment() {
		synchronized (this.mutex) {
		this.Valeur=this.Valeur+1;
		System.out.println("Le compteur est à "+this.Valeur);
		}
	}
	public Compteur() {
		super();
	}
	
}
