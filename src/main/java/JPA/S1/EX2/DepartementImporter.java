package JPA.S1.EX2;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.stream.Stream;

import javax.persistence.EntityManager;

import JDBC.S1.EX1.Commune;
import JDBC.S1.EX1.CommuneService;

public interface DepartementImporter {
	
public static void importDepartementFromCSVToDataBase(EntityManager EM,String pathparam) throws SQLException {
		
		String fileName = pathparam; //"src/main/resources/dep.csv";
		Path path = Path.of(fileName);
		
		Connection connexionJDBC = DriverManager.getConnection(
				"jdbc:mysql://127.0.0.1:3306/db_jdbc?user=antoine",
				"antoine",
				"user"
				
				);
		try (Stream<String> lines = Files.lines(path);) {

			lines.forEach(ligne->{
				String[] strSlpit= ligne.split(";");
				Departement currentLineDepartement = new Departement(strSlpit[0],strSlpit[1]);
				DepartementService.WriteCommuneInDataBase(EM, currentLineDepartement);
				
			});
	
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}

}
