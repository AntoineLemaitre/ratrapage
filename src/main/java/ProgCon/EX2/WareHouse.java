package ProgCon.EX2;

public class WareHouse {

	Object lock = new Object();
	int capacity;
	int CurrentNumberItems = 0;

	public boolean add() throws InterruptedException {
		synchronized (lock) {

			while (this.CurrentNumberItems == this.capacity) {
				lock.wait();
				System.out.println(Thread.currentThread().getName()+" is wainting");
			}

			if (this.CurrentNumberItems < this.capacity) {
				this.CurrentNumberItems = this.CurrentNumberItems + 1;
				System.out.println("Il y a " + this.CurrentNumberItems + "/" + this.capacity);
				lock.notifyAll();

				return true;
			} else {
				System.out.println("Il n' y a plus de place " + this.CurrentNumberItems + "/" + this.capacity);
				lock.wait();

				return false;
			}
		}
	}

	public boolean remove() throws InterruptedException {
		synchronized (lock) {
			while (this.CurrentNumberItems == 0) {
				lock.wait();
				System.out.println(Thread.currentThread().getName()+" is wainting");
			}
			if (this.CurrentNumberItems > 0) {
				this.CurrentNumberItems = this.CurrentNumberItems - 1;
				System.out.println("Il y a " + this.CurrentNumberItems + "/" + this.capacity);
				lock.notifyAll();
				return true;
			} else {
				System.out.println("Il n' y a pas d'Item " + this.CurrentNumberItems + "/" + this.capacity);
				lock.wait();
				return false;
			}
		}
	}

	public void content() {
		
			System.out.println("[CHECK] Il y a " + this.CurrentNumberItems + "/" + this.capacity);
		
	}

	public WareHouse(int capacity) {
		super();
		this.capacity = capacity;
	}

}
