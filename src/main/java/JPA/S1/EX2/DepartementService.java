package JPA.S1.EX2;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

public interface DepartementService {

	public static void WriteCommuneInDataBase(EntityManager EM, Departement departementToWrite) {
		EntityTransaction transaction = EM.getTransaction();
		transaction.begin();
		EM.persist(departementToWrite);
		transaction.commit();
	}
	
}
