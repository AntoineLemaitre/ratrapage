package JPA.S1.EX2;

import java.io.BufferedReader;
import java.nio.file.Files;
import java.nio.file.Path;

import javax.persistence.EntityManager;

import JPA.S1.EX2.Maire.Civilite;

public interface CommuneImporter {
	
	public static void importCommuneWithMaireFromCSVToDataBase(EntityManager EM,String FileName) {
		
		Path fileName = Path.of(FileName);
		try (BufferedReader reader= Files.newBufferedReader(fileName)) {
			
			String line = reader.readLine();
			line = reader.readLine();
			while (line!=null) {
				
				String[] strSplit=line.split(";");
				Departement CurrentDepartement = new Departement(strSplit[0].substring(1, 3),strSplit[1].replace("\"", "").replace("\"", ""));
				//System.out.println(CurrentDepartement);
				Maire CurrentLineMaire = new Maire(strSplit[5].replace("\"", "").replace("\"", ""), strSplit[4].replace("\"", "").replace("\"", ""),java.sql.Date.valueOf(strSplit[7]),Civilite.valueOf(strSplit[6].replace("\"", "").replace("\"", "")));
				//System.out.println(CurrentLineMaire);
				Commune CurrentLineCommune = new Commune(strSplit[0].substring(1, 3), strSplit[2].replace("\"", "").replace("\"", ""), CurrentLineMaire);
				//System.out.println(CurrentLineCommune);
				
				MaireService.WriteMaireInDataBase(EM, CurrentLineMaire);
				CommuneServie.WriteCommuneInDataBase(EM, CurrentLineCommune);
				
				line = reader.readLine();
			}
						
		} catch (Exception e) {
			// TODO: handle exception
		}
		
	}

	
}
