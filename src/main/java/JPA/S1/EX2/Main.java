package JPA.S1.EX2;

import java.io.BufferedReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;



import JPA.S1.EX2.Maire.Civilite;

public class Main {

	public static void main(String[] args) throws SQLException {
		// TODO Auto-generated method stub
		
		Connection connexionJPA = DriverManager.getConnection(
				"jdbc:mysql://127.0.0.1:3306/db_jpa?user=antoine",
				"antoine",
				"user"
				
				);
		
		System.out.println(connexionJPA);
		
		EntityManagerFactory EMF = Persistence.createEntityManagerFactory("jpa-test");
		System.out.println("EMF= "+EMF);
		EntityManager EM = EMF.createEntityManager();
		
		
		DepartementImporter.importDepartementFromCSVToDataBase(EM, "src/main/resources/dep.csv");
		CommuneImporter.importCommuneWithMaireFromCSVToDataBase(EM, "src/main/resources/data2.csv");

		EMF.close();
		connexionJPA.close();
		
		
	}

}
