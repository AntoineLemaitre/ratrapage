package ProgCon.EX3;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.Writer;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.function.Function;

import javax.imageio.spi.RegisterableService;
import javax.management.MBeanRegistrationException;

public class Server {
	
	public static void add(Socket socket,HashMap<String, Integer> registre) throws IOException {
		System.out.println("Accepting request "+Thread.currentThread().getName());
		
		InputStream inputStream = socket.getInputStream();
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));

        OutputStream outputStream = socket.getOutputStream();
        PrintWriter writer = new PrintWriter(new OutputStreamWriter(outputStream));

        String order = reader.readLine();
        System.out.println("ORDER= "+order);
        
        
        while (order != null) {
        	if (order.equals("QUIT")) {
        		writer.printf("[Server to CLient] Thread à tuer = %s\n", Thread.currentThread().getName());
        		writer.flush();
        		socket.close();
        		Thread.currentThread().interrupt();
        	}
        	if (order.startsWith("BUY")) {
        		
        		String[] str=order.split(" ");
				
        		writer.printf("[Server to CLient] ACHAT= %s\n", registre.get(str[1]));
        		writer.flush();
        		
        		registre.remove(str[1]);
			}
        	
        	if (order.equals("LIST")) {
        		
        		writer.printf("[Server to CLient] Registre= %s\n", registre);
        		writer.flush();
        		System.out.printf("START of LIST\n");
				System.out.printf("[Server to Server] Registre= %s\n", registre.toString());
				System.out.printf("END of LIST\n");
			}
        	
        	 
        	 if (order.startsWith("PUT")) {

        		 writer.printf("[Server to CLient] Received PUT order : %s\n ", order);
        		 writer.flush();
        		 System.out.printf("START of PUT\n");
        		 String[] strsplit=order.split(" ");
        		 System.out.printf("Product Name Added= %s\n", strsplit[1].toString());
        		 System.out.printf("Product Price Added= %s\n", strsplit[2].toString());
        		 registre.put(strsplit[1], Integer.parseInt(strsplit[2]));
        		 System.out.printf("Registre Update= %s\n", registre);
                 System.out.printf("END of PUT\n");

			}else if (order.startsWith("GET")) {
                writer.printf("[SERVER to CLient]Received GET order : %s\n", order);
                writer.flush();
                System.out.printf("[SERVER to SERVER]Received GET order : %s\n", order);
                
            } else if (order.equals("bye")) {
                System.out.printf("Closing connection\n");
                socket.close();
            }
            
            order = reader.readLine();
            System.out.println("Pret pour nouvelle requet");
            

        }
        
        System.out.println("END of Process");
        
	}

	
	public static void main(String[] args) throws IOException {
		ServerSocket server = new ServerSocket(8080);
		ExecutorService executeurClient = Executors.newFixedThreadPool(2);
		HashMap<String, Integer> registre = new HashMap<String, Integer>();
		
		System.out.println("Listening to request");
    	
    	Runnable ConnectionClient = () -> {   
    		
    		try {
    			System.out.println(Thread.currentThread().getName());
				add(server.accept(), registre);
				
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				try {
					server.close();
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
            
    		};
    		executeurClient.submit(ConnectionClient);
    		executeurClient.submit(ConnectionClient);
    		executeurClient.submit(ConnectionClient);
		
        

	}

}
