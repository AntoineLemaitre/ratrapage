package JPA.S2.EX5;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Stream;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import JPA.S1.EX2.Commune;
import JPA.S1.EX2.CommuneServie;
import JPA.S1.EX2.Departement;
import JPA.S1.EX2.DepartementService;
import JPA.S1.EX2.Maire;
import JPA.S1.EX2.MaireService;
import JPA.S2.EX5.Maire2.Civilite;;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		EntityManagerFactory EMF = Persistence.createEntityManagerFactory("jpa-test");
		System.out.println("EMF= "+EMF);
		EntityManager EM = EMF.createEntityManager();
		
		DepartementImporter2.importDepartementFromCSVToDataBase(EM, "src/main/resources/dep.csv", "src/main/resources/data2.csv");
		
	}
}
