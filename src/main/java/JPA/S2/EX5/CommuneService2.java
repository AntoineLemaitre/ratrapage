package JPA.S2.EX5;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

public interface CommuneService2 {
	
	public static void WriteCommuneInDataBase(EntityManager EM, Commune2 currentLineCommune) {
		EntityTransaction transaction = EM.getTransaction();
		transaction.begin();
		EM.persist(currentLineCommune);
		transaction.commit();
	}

}
