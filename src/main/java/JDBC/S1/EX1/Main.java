package JDBC.S1.EX1;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.util.stream.Stream;

public class Main {

	public static void main(String[] args) throws SQLException {
		// TODO Auto-generated method stub
		String fileName = "src/main/resources/data.csv";
		//je considère que la table est déjà créer au bon format 
		CommuneImporter.importCommuseFromCSVToDataBase(fileName);
		Connection connexionJDBC = DriverManager.getConnection(
				"jdbc:mysql://127.0.0.1:3306/db_jdbc?user=antoine",
				"antoine",
				"user"
				
				);
		System.out.println(CommuneService.getCommuneByName(connexionJDBC, "SANNOIS"));
		System.out.println("Il y a "+CommuneService.countCommuneByCP(connexionJDBC, "95")+" communes dans le 95 !");
		
	}

}
