package JPA.S2.EX5;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;



@Entity
public class Maire2 implements Serializable{
	
	@Id @GeneratedValue(strategy = GenerationType.SEQUENCE)
	int id;
	String prenom;
	String nom;
	Date   dateDeNaissance;
	public enum  Civilite{
		  M,
		  F
		}
	Civilite civilité;
	
	public Maire2(String prenom, String nom) {
		super();
		this.prenom = prenom;
		this.nom = nom;
	}
	

	public Maire2(String prenom, String nom, Date dateDeNaissance, Civilite civilité) {
		super();
		this.prenom = prenom;
		this.nom = nom;
		this.dateDeNaissance = dateDeNaissance;
		this.civilité = civilité;
	}


	@Override
	public String toString() {
		return prenom +" "+ nom + " né(e) le " + dateDeNaissance + " "+ civilité ;
	}


	public String getPrenom() {
		return prenom;
	}


	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}


	public String getNom() {
		return nom;
	}


	public void setNom(String nom) {
		this.nom = nom;
	}


	public Date getDateDeNaissance() {
		return dateDeNaissance;
	}


	public void setDateDeNaissance(Date dateDeNaissance) {
		this.dateDeNaissance = dateDeNaissance;
	}


	public Civilite getCivilité() {
		return civilité;
	}


	public void setCivilité(Civilite civilité) {
		this.civilité = civilité;
	}


	public Maire2() {
		
	}
	

}
