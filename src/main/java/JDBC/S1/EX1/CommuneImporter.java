package JDBC.S1.EX1;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;
import java.util.stream.Stream;

public interface CommuneImporter {
	
	public static void importCommuseFromCSVToDataBase(String pathparam) throws SQLException {
		
		String fileName = pathparam; //"src/main/resources/data.csv";
		Path path = Path.of(fileName);
		
		Connection connexionJDBC = DriverManager.getConnection(
				"jdbc:mysql://127.0.0.1:3306/db_jdbc?user=antoine",
				"antoine",
				"user"
				
				);
		try (Stream<String> lines = Files.lines(path);) {

			lines.forEach(ligne->{
				Commune CommuneFromLine = new Commune(ligne);
				
						try {
							CommuneService.WriteCommuneInDataBase(connexionJDBC,CommuneFromLine);
						} catch (SQLException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
				
			});
	
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}

}
