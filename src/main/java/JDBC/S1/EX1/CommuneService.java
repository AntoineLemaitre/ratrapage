package JDBC.S1.EX1;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public interface CommuneService {
	
		
	public static  void WriteCommuneInDataBase(Connection connexion,Commune commune) throws SQLException {
		
		String sql = "insert into Commune values ('"+commune.codePostal+"','"+commune.codeINSEE+"','"+commune.nomCommune+"','"+commune.libelleAcheminement+"')"; 
		//Statement statment = connexion.createStatement();
		//statment.executeQuery(sql);
		connexion.prepareCall(sql).execute();
		
		
	}
	
	public static ArrayList<Commune> getCommuneByName(Connection connexion,String name) throws SQLException {
		
		String sql = "SELECT * from Commune WHERE nomCommune LIKE '"+name+"';"; 
		Statement statment = connexion.createStatement();
		ResultSet result=statment.executeQuery(sql);
		ArrayList<Commune> listeCommune = new ArrayList<Commune>();
		while (result.next()) {
			
			Commune CurrentCommune = new Commune(result.getString("codeINSEE"),result.getString("nomCommune"),result.getString("codePostal"),result.getString("libelleAcheminement"));
			//System.out.println(CurrentCommune);
			listeCommune.add(CurrentCommune);
		}
		
		
		return listeCommune;
		
	}
	
	public static int countCommuneByCP(Connection connexion,String codePostal) throws SQLException {
		
		String sql = "SELECT * from Commune WHERE codePostal LIKE '"+codePostal+"%';";
		Statement statment = connexion.createStatement();
		ResultSet result=statment.executeQuery(sql);
		ArrayList<Commune> listeCommune = new ArrayList<Commune>();
		while (result.next()) {
			
			Commune CurrentCommune = new Commune(result.getString("codeINSEE"),result.getString("nomCommune"),result.getString("codePostal"),result.getString("libelleAcheminement"));
			//System.out.println(CurrentCommune);
			listeCommune.add(CurrentCommune);
		}
		return listeCommune.size();
			
	}
	

}
