package ProgCon.EX1;

import java.util.concurrent.atomic.AtomicLong;
import java.util.function.LongUnaryOperator;

public class AtomicComteur {

	Object mutex=new Object();
	AtomicLong Valeur= new AtomicLong();
	
	public void Increment() {
		//synchronized (this.mutex) {
		this.Valeur.incrementAndGet();
		System.out.println("Le compteur Atomic est à "+this.Valeur);
		//}
	}
	public AtomicComteur() {
		super();
	}
}
