package JPA.S2.EX5;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;



public interface DepartementService2 {
	
	public static void WriteCommuneInDataBase(EntityManager EM, Departement2 departementToWrite) {
		EntityTransaction transaction = EM.getTransaction();
		transaction.begin();
		EM.persist(departementToWrite);
		transaction.commit();
	}

}
