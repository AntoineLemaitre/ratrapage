package JPA.S2.EX5;

import java.io.Serializable;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.print.attribute.HashAttributeSet;

import JPA.S2.EX5.Commune2;

@Entity
public class Departement2 implements Serializable{
	
	@Id 
	String numero;
	String nom;
	@ManyToMany (cascade = CascadeType.ALL)
	Set<Commune2> communes= new HashSet<Commune2>();
	
	public String getNumero() {
		return numero;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public Departement2() {
		super();
	}
	public Departement2(String numero, String nom) {
		super();
		this.numero = numero;
		this.nom = nom;
	}
	
	@Override
	public String toString() {
		return "Departement2 [numero=" + numero + ", nom=" + nom + ", communes=" + communes + "]";
	}
	public Set<Commune2> getCommunes() {
		return communes;
	}
	public void setCommunes(Set<Commune2> communes) {
		this.communes = communes;
	}
	
	

}
