package JDBC.S1.EX1;

public class Commune {
	
	String codeINSEE;
	String nomCommune;
	String codePostal;
	String libelleAcheminement;
	
	public Commune LigneToCommune(String ligne ) {
		
		String[] ligneSlipted = ligne.split(";");
		return new Commune(ligneSlipted[0],ligneSlipted[1],ligneSlipted[2],ligneSlipted[3]);
		
	}
	
	@Override
	public String toString() {
		return nomCommune + " à comme code postal " + codePostal;
	}
	public String getCodeINSEE() {
		return codeINSEE;
	}
	public void setCodeINSEE(String codeINSEE) {
		this.codeINSEE = codeINSEE;
	}
	public String getNomCommune() {
		return nomCommune;
	}
	public void setNomCommune(String nomCommune) {
		this.nomCommune = nomCommune;
	}
	public String getCodePostal() {
		return codePostal;
	}
	public void setCodePostal(String codePostal) {
		this.codePostal = codePostal;
	}
	public String getLibelleAcheminement() {
		return libelleAcheminement;
	}
	public void setLibelleAcheminement(String libelleAcheminement) {
		this.libelleAcheminement = libelleAcheminement;
	}
	

	public Commune(String ligneCSV) {
		
		super();
		
		String[] ligneSlipted = ligneCSV.split(";");
		this.codeINSEE = ligneSlipted[0];
		this.nomCommune = ligneSlipted[1];
		this.codePostal = ligneSlipted[2];
		this.libelleAcheminement = ligneSlipted[3];
	}

	public Commune(String codeINSEE, String nomCommune, String codePostal, String libelleAcheminement) {
		super();
		this.codeINSEE = codeINSEE;
		this.nomCommune = nomCommune;
		this.codePostal = codePostal;
		this.libelleAcheminement = libelleAcheminement;
	}
	

}
