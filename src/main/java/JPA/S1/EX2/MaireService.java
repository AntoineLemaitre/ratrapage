package JPA.S1.EX2;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

public interface MaireService {
	
	public static void WriteMaireInDataBase(EntityManager EM, Maire Maire  ) {
		EntityTransaction transaction = EM.getTransaction();
		transaction.begin();
		EM.persist(Maire);
		transaction.commit();
	}

}
