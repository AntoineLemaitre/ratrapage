package JPA.S2.EX6;

import java.sql.SQLException;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class Main {
	
	private static void DoforDepwithNumber(String nbdep) throws SQLException {
		// TODO Auto-generated method stub
		System.out.println("Il y a "+DepartementRequest.getNumberOfTownWithDepartementNumber(nbdep)+" ville dans le "+nbdep+"!");
		System.out.println(DepartementRequest.getNumberOldestMayor(nbdep)+" est le/la plus agé(e) du "+nbdep);
		System.out.println(DepartementRequest.getRationMenWomen(nbdep)*100+" % de femme Maire dans le "+nbdep);
	}

	public static void main(String[] args) throws SQLException {
		// TODO Auto-generated method stub
		
		// les entitées sont crées avec l'exo d'avant
		Main.DoforDepwithNumber("95");
		Main.DoforDepwithNumber("94");
		Main.DoforDepwithNumber("75");
		Main.DoforDepwithNumber("93");
	}

}
