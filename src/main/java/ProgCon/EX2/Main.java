package ProgCon.EX2;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		WareHouse CurrentWareHouse = new WareHouse(10);
		Runnable AddWareHouse = () -> {
			try {
				CurrentWareHouse.add();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		};
		Runnable RemoveWareHouse = () -> {
			try {
				CurrentWareHouse.remove();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		};
		Runnable ContentWareHouse = () -> {
			CurrentWareHouse.content();
		};

		ExecutorService executeur = Executors.newFixedThreadPool(10);
		for (int i = 0; i < 100; i++) {

			executeur.submit(AddWareHouse);
			if (i < 95) {
				executeur.submit(RemoveWareHouse);
			}

		}
		executeur.shutdown();
		CurrentWareHouse.content();

	}

}
